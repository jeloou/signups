$(function() {
  $('form.form-signup').submit(function(e) {
    var data, _data;
    
    e.preventDefault();
    data = $(e.target).serializeArray();
    data.gende = $('#gender button.active').val();
    
    _data = {};
    $.each(data, function(k, field) {
      _data[field.name] = field.value;
    });
    
    $.ajax({
      type: 'POST',
      url: '/users/',
      contentType: 'application/json; charset=utf-8',
      dataType: 'json',
      data: JSON.stringify(_data),
      success: function(user) {
	console.log(user);
      },
      error: function(res, b, c) {
	alert(res.responseText);
      }
    });
  });
});