#config=utf-8

from django.core.mail import EmailMultiAlternatives, send_mail
from django.template.loader import get_template 
from django.template import Context
import settings

def send(template, context, options={}):
    plain_text = get_template('%s.txt' % template)
    html = get_template('%s.html' % template)

    from_email = options.get('from_email', getattr(settings, 'EMAIL_HOST_USER'))
    subject = options.get('subject', '')
    to = options.get('to', [])

    context = Context(context)
    text_content = plain_text.render(context)
    html_content = html.render(context)
    
    if type(to) != type([]):
        to = [to]
        
    email = EmailMultiAlternatives(subject, text_content, from_email, to)
    email.attach_alternative(html_content, 'text/html')
    email.send()

    
    

