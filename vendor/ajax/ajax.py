#coding=utf-8
from django.utils import simplejson as json
from django.http import HttpResponseBadRequest, Http404, HttpResponse, HttpResponseRedirect, QueryDict
from django.utils.timezone import utc
from django.db import transaction

from datetime import datetime
import settings
import hashlib 
import random 
import time 
import re

class RESTMiddleware(object):
    def process_request(self,request):
        request.PUT = QueryDict('')
        method = request.META.get('REQUEST_METHOD','').upper()
        if method == 'PUT':
            self.handle_PUT(request)
            
    def parse_multipart(self, request):
        return request.parse_file_upload(request.META, request)

    def parse_request(self, request):
        if re.match('^multipart/form-data', request.META['CONTENT_TYPE']):
            return self.parse_multipart(request)

        elif re.match('^application/x-www-form-urlencoded', request.META['CONTENT_TYPE']):
            return QueryDict(request.body), request.FILES
        
        else:
            return request.POST, request.FILES
        
    def handle_PUT(self, request):
        request.POST, request._files = self.parse_request(request)

def bind(router, env, other_handlers=[]):
    def _router(request, *args, **kwargs):
        handlers = {}
        for k, v in env.items():
            if k in ('create', 'read', 'fetch', 'update', 'delete'):
                handlers[k] = v

        for k in other_handlers:
            handler = env.get(k)
            if handler:
                handlers[k] = handler
                
        request.parent_id = kwargs.pop('parent_id', None)
        kwargs['handlers'] = handlers

        return router(request, *args, **kwargs)
    return _router

def _parse_json_request(request):
    if re.match('^application/json', request.META.get('CONTENT_TYPE', '')):
        try:
            request.POST = json.loads(request.body)
        except ValueError:
            return False
    else:    
        if 'data' in request.POST:
            try:
                request.POST = json.loads(request.POST['data'])
            except ValueError:
                return False

    return len(request.POST) or len(request.FILES)

def router(request, handlers, id=None, single_entity=False):
    if not single_entity:
        if request.method in ('GET', 'POST'):
            method = handlers.get('fetch' if request.method == 'GET' else 'create')

            if method:
                if request.method == 'POST':
                    if not _parse_json_request(request):
                        return error(400, {
                            'message': 'Missing or invalid POST body'
                        })
                return method(request)
        raise Http404()
    else:
        methods = {
            'DELETE': 'delete',
            'PUT': 'update',
            'GET': 'read'
        }
        
        if request.method in ('GET', 'PUT', 'DELETE'):
            method = handlers.get(methods[request.method])
            if method:
                if request.method == 'PUT':
                    if not _parse_json_request(request):
                        return error(400, {
                            'message': 'Missing or invalid PUT body'
                        })
                return method(request, id)
        raise Http404()

def respond(data):
    return HttpResponse(json.dumps(data), mimetype='application/json')

def error(status, content=None):
    return HttpResponse(
        status=status, 
        content=json.dumps(content),
        mimetype='application/json')

def login_required(view):
    def _wrapper(request, *args, **kwargs):
        if not request.user.is_authenticated():
            return error(401, {
                'message': 'You\'re not allowed to do that'
            })

        return view(request, *args, **kwargs)
    return _wrapper
