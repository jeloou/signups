#coding=utf-8

from django.shortcuts import redirect, render_to_response
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login as auth_login, logout as auth_logout
from django.views.decorators.http import require_GET

from .models import Profile
from vendor import ajax

router = ajax.bind(ajax.router, locals())
def create(request):
    if request.user.is_authenticated():
        return ajax.error(400, {
            'message': 'You can\'t perform this action'
        })
    
    profile = Profile.create(request.POST, request.FILES)
    if not profile.is_valid():
        return ajax.error(400, {
            'message': profile.errors
        })

    return ajax.respond(profile.to_dict())

@require_GET
def login(request):
    if request.user.is_authenticated():
        return redirect('/')
    
    return render_to_response('login.html')

@login_required
def logout(request):
    auth_logout(request)
    return redirect('/')

def signup(request):
    if request.user.is_authenticated():
        return redirect('/')

    return render_to_response('signup.html')
