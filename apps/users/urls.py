#coding=utf-8

from django.conf.urls import patterns, url, include

urlpatterns = patterns('apps.users.views',
    url(r'^/?$', 'router'),
    url(r'^login/?$', 'login'),
    url(r'^logout/?$', 'logout'),
    url(r'^signup/?', 'signup'),
    url(r'^', include('social.apps.django_app.urls', namespace='social')),
    url(r'^(?P<id>\w+)/?$', 'router', {'single_entity': True}),
)
