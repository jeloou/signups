#coding=utf-8

from social.exceptions import AuthException 

def user_details(strategy, details, uid, user=None, *args, **kwargs):
    if user:
        data = strategy.request_data()
        
        count = strategy.storage.user.objects.filter(user_id=user.id).count()
        if count > 1:
            return

        if strategy.backend.name == 'email':
            if not 'sign_up' in data:
                return
            
        protected = strategy.setting('PROTECTED_USER_FIELDS', [])
        keep = ('username', 'id', 'pk') + tuple(protected)
        changed = False
        
        for k, v in details.items():
            if k not in keep and hasattr(user, k):
                if v and v != getattr(user, k, None):
                    try:
                        setattr(user, k, v)
                        changed = True
                    except AttributeError:
                        pass
        
        if changed:
            strategy.storage.user.changed(user)
            
def user_password(strategy, user=None, is_new=False, *args, **kwargs):
    if strategy.backend.name != 'email':
        return
    
    password = strategy.request_data()['password']
    if not is_new:
        if not (user.has_usable_password() and user.check_password(password)):
            raise AuthException(
                strategy.backend, 
                'Wrong email or password, try again'
            )
        return
    
    user.set_password(password)
    user.save()
    
def validate_email(strategy, user=None, is_new=False, *args, **kwargs):
    if not user:
        return
    
    data = strategy.request_data()
    if 'sign_up' in data and user:
        raise AuthException(
            strategy.backend,
            'The given email address is already in use'
        )
