#config=utf-8 

from django.db import models
from django.contrib.auth.models import AbstractUser

class Profile(AbstractUser):
    @classmethod
    def get(cls, **kwargs):
        try:
            return cls.objects.get(**kwargs)
        except cls.DoesNotExist:
            return None

    @classmethod
    def create(cls, data, files=None):
        from .forms import ProfileForm
        
        form = ProfileForm(data, files)
        if not form.is_valid():
            return form
        
        data = form.cleaned_data
        profile = cls(**data)
        
        #if data['photo']:
        #    profile.update_photo(data['photo'])
        
        profile.set_password(data['password'])
        profile.has_password = True
        
        profile.save()
        
        setattr(profile, 'is_valid', lambda: True)
        return profile
    
    @classmethod
    def exists(cls, **kwargs):
        return cls.objects.filter(**kwargs).count() >= 1

    def __unicode__(self):
        return u'%s %s'%(self.first_name, self.last_name)
    
    def to_dict(self):
        return {
            'id': self.id,
            'username': self.username,
            'email': self.email,
            'name': self.__unicode__(),
            'first_name': self.first_name,
            'last_name': self.last_name
        }
    
