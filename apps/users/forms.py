#config=utf-8

from django import forms
from .models import Profile

class LoginForm(forms.Form):
    email = forms.EmailField()
    password = forms.CharField()
    
class ProfileForm(forms.ModelForm):
    first_name = forms.CharField(max_length=35)
    last_name = forms.CharField(max_length=35)
    email = forms.EmailField()
    
    class Meta:
        model = Profile
        fields = (
            'username', 
            'first_name', 
            'last_name', 
            'password',
            'email'
        )
        
    def clean_email(self):
        email = self.cleaned_data['email']
        
        if Profile.exists(email=email):
            raise forms.ValidationError('That email is already in use')
        return email
    
