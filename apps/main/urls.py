from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
import settings

urlpatterns = patterns('apps.main.views',
    ('^/?$', 'router'),
)
