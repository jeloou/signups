#coding=utf-8

from django.contrib.auth.decorators import login_required
from django.shortcuts import render_to_response
from django.utils.translation import ugettext as _
from django.utils import translation
import settings

@login_required
def router(request):
    return render_to_response('home.html', {
        'user': request.user
    })
