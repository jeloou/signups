#coding=utf-8

from django import forms

def is_email(email):
    try:
        forms.EmailField().clean(email)
        return True
    except forms.ValidationError:
        return False
