from django.conf.urls import patterns, include, url
from django.conf.urls.i18n import i18n_patterns
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
import settings

urlpatterns = i18n_patterns('',
    ('', include('apps.main.urls')),
    ('^users/', include('apps.users.urls'))
)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()

