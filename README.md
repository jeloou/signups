# signups - An multilingual app with multiple ways to sign up

## Getting started

Run locally:

    $ git clone git@bitbucket.org:jeloou/signups.git
    $ cd signups
    $ virtualenv --prompt='(signups)' .env.d
    $ source .env.d/bin/activate
    $ pip install -r config/requirements.txt
    $ ./manage.py syncdb
    $ ./manage.py runserver

Visit <http://localhost:8000> to marvel at your work. 

You should have a new and shining instance running locally. You can create a new user using `/users/signup`

## What's missing 

Something missing? [please raise an issue](https://bitbucket.org/jeloou/signups/issues?status=new&status=open)

