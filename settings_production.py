#config=utf-8

import os
import sys
import settings

try:
    import djcelery
except ImportError, e:
    sys.stderr.write('Error: Seems like you\'ve some missing dependencies. %s\n' % e.message)
    sys.exit(1)
    
INSTALLED_APPS = tuple(list(settings.INSTALLED_APPS) + [
   'storages',
   'djsupervisor',
   'djcelery',
   'djcelery_email',
   'kombu.transport.django'])

SUPERVISOR_CONFIG_FILE = 'config/supervisord.conf'

# MySQL database configuration
DATABASE_NAME = os.environ['DATABASE_NAME']
DATABASE_USER = os.environ['DATABASE_USER']
DATABASE_PASSWORD = os.environ['DATABASE_PASSWORD']

# Celery configuration, including database
BROKER_BACKEND = 'redis'
BROKER_HOST = os.environ['REDIS_HOST']
BROKER_PORT = os.environ['REDIS_PORT']
BROKER_VHOST = os.environ['REDIS_DB']

CELERY_RESULT_BACKEND = 'redis'
REDIS_HOST = os.environ['REDIS_HOST'] 
REDIS_PORT = os.environ['REDIS_PORT']
REDIS_DB = os.environ['REDIS_DB']

CELERYBEAT_PIDFILE = '/tmp/celerybeat.pid'
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERYBEAT_SCHEDULE = {} # Will add tasks later
    
# django-celery-email configuration
EMAIL_BACKEND = 'djcelery_email.backends.CeleryEmailBackend'

# email credentials
EMAIL_USE_TLS = True
EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_PORT = os.environ['EMAIL_PORT']
EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = os.environ['EMAIL_HOST_PASSWORD']

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': DATABASE_NAME,
        'USER': DATABASE_USER,
        'PASSWORD': DATABASE_PASSWORD,
    }
}

# Usin S3 as static files storage, loading configuration
DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'

AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_ACCESS_KEY_ID']

S3_URL = 'http://%s.s3.amazonaws.com/' % AWS_STORAGE_BUCKET_NAME
STATIC_URL = S3_URL

APPEND_SLASH = False

SOCIAL_AUTH_FACEBOOK_KEY = os.environ['SOCIAL_AUTH_FACEBOOK_KEY'] 
SOCIAL_AUTH_FACEBOOK_SECRET = os.environ['SOCIAL_AUTH_FACEBOOK_SECRET']
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email']

SOCIAL_AUTH_GOOGLE_OAUTH2_KEY = os.environ['SOCIAL_AUTH_GOOGLE_OAUTH2_KEY'] 
SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET = os.environ['SOCIAL_AUTH_GOOGLE_OAUTH2_SECRET']
SOCIAL_AUTH_GOOGLE_OAUTH2_SCOPE = ['email']

djcelery.setup_loader()

